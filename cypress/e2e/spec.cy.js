describe('CADASTRO ALURA PIC', () => {
  beforeEach(() => {
      cy.visit('https://alura-fotos.herokuapp.com/#/home')
  })

  it('Validar tela de registro', () => {
      cy.contains('a', 'Register now').click()
      cy.contains('h4', 'Register to embrace a new world!').should('be.visible');
  })

  it('Validar msg validação', () => {
      cy.contains('a', 'Register now').click()
      cy.contains('h4', 'Register to embrace a new world!').should('be.visible');
      cy.contains('button','Register').click()
      cy.contains('ap-vmessage','Email is required!').should('be.visible')
      cy.contains('button','Register').click()
      cy.contains('ap-vmessage','Full name is required!').should('be.visible')
      cy.contains('ap-vmessage','User name is required!').should('be.visible')
      cy.contains('ap-vmessage','Password is required!').should('be.visible')
     
     
  })

  it('Validar msg e-mail inválido', () => {
      cy.contains('a', 'Register now').click()
      cy.contains('h4', 'Register to embrace a new world!').should('be.visible');
      cy.get('input[placeholder="email"]').type('rafael')
      cy.contains('button','Register').click()
      cy.contains('ap-vmessage','Invalid e-mail').should('be.visible')
  })
})
